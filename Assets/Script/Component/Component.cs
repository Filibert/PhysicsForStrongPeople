﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Script.Component
{
    public interface Component
    {
        string Tag { get; set; }
        Boolean HasAForce { get; set; }
        GameObjectNewGen GameObjectNewGen { get; set; }
        Vector3 Force { get; set; }
        bool Update();
    }
}