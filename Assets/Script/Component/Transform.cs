﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Assets.Script.Helper;
using Script.Model.Math;
using UnityEngine;

namespace Assets.Script.Component
{
    public class Transform : Component
    {
        public string Tag { get; set; }
        public bool HasAForce { get; set; }
        public GameObjectNewGen GameObjectNewGen { get; set; }
        public Vector3 Force { get; set; }

        public Vector3 Position = new Vector3();
        public OrthonormalBasis LocalBasis = new OrthonormalBasis();
        public OrthonormalBasis ParentBasis = new OrthonormalBasis();
        public Vector3 EulerAngle = new Vector3();
        public Vector3 Scale = new Vector3(){x = 1, y = 1, z = 1};

        public UnityEngine.Quaternion Orientation =  new UnityEngine.Quaternion();

        public Transform()
        {
            HasAForce = false;
            Force = Vector3.zero;
        }

        public void RotateLocal(float x, float y, float z)
        {
            LocalBasis = RotationHelper.Rotate(x, y, z, ParentBasis);
            Matrix newBasisMatrix = new Matrix(LocalBasis);
            int i = 0;
            foreach (var gameObject in GameObjectNewGen.GameObjects)
            {
                gameObject.transform.position = (newBasisMatrix * new Matrix(GameObjectNewGen.GameObjectsNormalizedPosition[i] - Position)).ToVector3() + Position;
                Debug.Log(gameObject.transform.position);
                RotationHelper.RotateAndChangeOrientation(x, y, z, ParentBasis, gameObject);
                i++;
            }
        }
        public void Rotate(float x, float y, float z)
        {   
            EulerAngle = new Vector3(x , y, z);
            LocalBasis = RotationHelper.Rotate(x, y, z, new OrthonormalBasis()
            {
                xAxis = Vector3.right,
                yAxis = Vector3.up,
                zAxis = Vector3.forward
            });
            Matrix newBasisMatrix = new Matrix(LocalBasis);
            int i = 0;
           // GameObjectNewGen.rigidBody.ObjectInertia = newBasisMatrix.Inverse() * GameObjectNewGen.rigidBody.ObjectInertia;
            foreach (var gameObject in GameObjectNewGen.GameObjects)
            {
                gameObject.transform.position = (newBasisMatrix * new Matrix(GameObjectNewGen.GameObjectsNormalizedPosition[i] - Position)).ToVector3() + Position;
                RotationHelper.RotateAndChangeOrientation(x, y, z, ParentBasis, gameObject);
                i++;
            }

        }

        public void Translate(Vector3 translationVector)
        {
            Position.x += translationVector.x;
            Position.y += translationVector.y;
            Position.z += translationVector.z;
            
            List<Vector3> newList = new List<Vector3>();
            int i = 0;
            foreach (var vector in GameObjectNewGen.GameObjectsNormalizedPosition)
            {
                GameObjectNewGen.GameObjects[i].transform.position = vector + translationVector;
                newList.Add(vector+translationVector);
                i++;
            }
            GameObjectNewGen.GameObjectsNormalizedPosition = newList;
        }

        public void Scaling(Vector3 scalingVector)
        {
            Scale = scalingVector;
            ScaleHelper.ScalePosition(scalingVector, LocalBasis.xAxis);
            ScaleHelper.ScalePosition(scalingVector, LocalBasis.yAxis);
            ScaleHelper.ScalePosition(scalingVector, LocalBasis.zAxis);
            int i = 0;
            foreach (var gameObject in GameObjectNewGen.GameObjects)
            {
                gameObject.transform.position = ScaleHelper.ScalePosition(scalingVector, GameObjectNewGen.GameObjectsNormalizedPosition[i]);
                gameObject.transform.localScale = ScaleHelper.ScalePosition(scalingVector, GameObjectNewGen.GameObjectsInitialScale[i]) ;
                i++;
            }

        }

        public Vector3 TransformInverse(Vector3 vector)
        {
            Vector3 tmp = vector;
            tmp.x -= Position.x;
            tmp.y -= Position.y;
            tmp.z -= Position.z;

            return new Vector3(
                tmp.x * LocalBasis.xAxis.x +
                tmp.y * LocalBasis.xAxis.y +
                tmp.z * LocalBasis.xAxis.z,
                tmp.x * LocalBasis.yAxis.x +
                tmp.y * LocalBasis.yAxis.y +
                tmp.z * LocalBasis.yAxis.z,
                tmp.x * LocalBasis.zAxis.x +
                tmp.y * LocalBasis.zAxis.y +
                tmp.z * LocalBasis.zAxis.z
                );
        }

        public Boolean Update()
        {
            Translate(GameObjectNewGen.rigidBody.Velocity);
            Rotate(EulerAngle.x + GameObjectNewGen.rigidBody.AngularVelocity.x, EulerAngle.y + GameObjectNewGen.rigidBody.AngularVelocity.y, EulerAngle.z + GameObjectNewGen.rigidBody.AngularVelocity.z);
            return true;
        }
    }
}    