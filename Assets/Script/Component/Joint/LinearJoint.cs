﻿using System;
using Script.Model.Math;
using UnityEngine;
using UnityEngine.Assertions.Comparers;

namespace Assets.Script.Component.Joint
{
    public class LinearJoint : Joint
    {
        public float MaxLength;
        public override Vector3 Force
        {
            get
            {
                Vector3 AnchorInWorld = (new Matrix(ConnectedBody.GameObjectNewGen.transform.LocalBasis) * new Matrix(Anchor)).ToVector3() + ConnectedBody.GameObjectNewGen.transform.Position;
                Vector3 ConnectedAnchorInWorldSpace = (new Matrix(GameObjectNewGen.transform.LocalBasis) * new Matrix(ConnectedAnchor)).ToVector3() + GameObjectNewGen.transform.Position;
                Debug.Log(AnchorInWorld);
                if (MaxLength <= (AnchorInWorld - ConnectedAnchorInWorldSpace).magnitude)
                {
                    float accMagnitude =
                    ((new Vector3(0, -9.81f * Convert.ToInt32(GameObjectNewGen.rigidBody.Gravity), 0) +
                      GameObjectNewGen.rigidBody.Acceleration) * GameObjectNewGen.rigidBody.Mass).magnitude;
                    float distance = (AnchorInWorld - ConnectedAnchorInWorldSpace).magnitude - MaxLength;
                    return (accMagnitude + 1000*distance) * (AnchorInWorld - ConnectedAnchorInWorldSpace).normalized - GameObjectNewGen.rigidBody.Velocity;

                }
                return Vector3.zero;
            }
            set { Force = value; }
        }

        public LinearJoint(float maxLength, GameObjectNewGen go, Vector3 Anchor, Vector3 ConnectedAnchor, GameObjectNewGen ObjectLinked)
        {
            MaxLength = maxLength;
            base.Anchor = Anchor;
            base.ConnectedAnchor = ConnectedAnchor;
            ConnectedBody = ObjectLinked.rigidBody;
            GameObjectNewGen = go;
        }

        public override bool Update()
        {
            
            return true;
        }
    }
}
