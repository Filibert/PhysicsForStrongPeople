﻿using System;
using UnityEngine;

namespace Assets.Script.Component.Joint
{
    public abstract class Joint : Component
    {
        public string Tag { get; set; }
        public bool HasAForce { get; set; }
        public GameObjectNewGen GameObjectNewGen { get; set; }
        public abstract Vector3 Force { get; set; }
        public abstract Boolean Update();

        public Vector3 Anchor { get; set; }
        public Vector3 ConnectedAnchor { get; set; }

        public RigidBody ConnectedBody;

        public Vector3 Axis { get; set; }
        
        public float BreakForce;
        public float BreakTorque;

        public Joint()
        {
            HasAForce = true;
        }
    }
}