﻿using System;
using System.Collections.Generic;
using Assets.Script.Physics;
using UnityEngine;

namespace Assets.Script.Component.Joint
{
    public class SpringJoint : Joint
    {        
        public RigidBody ConnectedBody;
        public SpringForce springForce;
        public float MinDistance;
        public float MaxDistance;
        
        public override Vector3 Force
        {
            get
            {
                Axis = (Anchor - GameObjectNewGen.transform.Position) * springForce.K;
                return Axis;
            }
            set { Force = value; }
        }
        
        public SpringJoint(float k, float c, GameObjectNewGen go, RigidBody connectedBody)
        {
            ConnectedBody = connectedBody;
            GameObjectNewGen = go;
            Anchor = GameObjectNewGen.transform.Position;
            springForce = new SpringForce
            {
                K = k,
                C = c,
                RestingLentgh = 
                    (GameObjectNewGen.transform.Position - ConnectedBody.GameObjectNewGen.transform.Position).magnitude
            };
        }
        public override Boolean Update()
        {
            springForce.ActualLentgh = 
                (GameObjectNewGen.transform.Position - ConnectedBody.GameObjectNewGen.transform.Position).magnitude;

            return true;
        }
    }
}