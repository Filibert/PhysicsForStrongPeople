﻿using System;
using System.Security.Policy;
using Assets.Script.Physics;
using Script.Helper;
using Script.Model.Math;
using UnityEngine;

namespace Assets.Script.Component
{
    public class RigidBody : Component
    {
        private float mass;
        public float Mass
        {
            get
            {
                return this.mass;
            }

            set
            {
                this.mass = value;
                this.InverseMass = 1 / value;
            }
        }
        
        public float InverseMass;
        public float Volume;
        public float RestitutionCoeff;
        public CollisionType CollisionType;
        public Vector3 Velocity, Acceleration, Direction ;

        public float LinearDamping = 1, RotationalDamping = 1;
        
        
        public Matrix ObjectInertia;
        
        public Vector3 AngularVelocity, AngularAcceleration;

        public string Tag { get; set; }
        public bool HasAForce { get; set; }
        public bool Gravity = false;

        public GameObjectNewGen GameObjectNewGen { get; set; }
        public Vector3 Force { get; set; }

        public RigidBody()
        {
            HasAForce = false;
            Force = Vector3.zero;
        }

        public void ApplyLinearForce(Vector3 force)
        {
            Acceleration += force / Mass;
        }

        public void ApplyRotationalForces(Vector3 impactPoint, Vector3 force)
        {
            ApplyLinearForce(force);
            Torque torque = new Torque(){ImpactPoint = impactPoint, Force = force};
                Matrix rotation = ObjectInertia.Inverse() * new Matrix(torque.TorqueVector) ;
            AngularAcceleration += rotation.ToVector3();
            //for now we consider all object as     
        }
    
        public void UpdateVelocity()
        {
            if(Gravity) 
               ApplyGravity();
            Velocity = (Velocity * Mathf.Pow(LinearDamping,Time.deltaTime) + (Acceleration * (Time.deltaTime * Time.deltaTime)/2));
            AngularVelocity = AngularVelocity * Mathf.Pow(RotationalDamping,Time.deltaTime) + AngularAcceleration;
            Acceleration = Vector3.zero;
            AngularAcceleration = Vector3.zero;
        }

        public void ApplyGravity()
        {
            ApplyRotationalForces(new Vector3(0,0,0), Vector3.down*9.81f*Mass);
           // ApplyLinearForce(new Vector3(0,-9.81f*Mass,0));
        }
        
        public Boolean Update()
        {
            UpdateVelocity();
            return true;
        }
    }
}