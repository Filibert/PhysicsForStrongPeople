﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Assets.Script.Component;
using Assets.Script.Component.Joint;
using Assets.Script.Helper;
using Script.Model.Math;
using UnityEngine;
using SpringJoint = Assets.Script.Component.Joint.SpringJoint;

namespace Assets.Script
{
     

    public abstract class GameObjectNewGen 
    {
        public List<GameObject> GameObjects { get; set; }
        public List<Vector3> GameObjectsNormalizedPosition = new List<Vector3>();
        public List<Vector3> GameObjectsInitialScale = new List<Vector3>();

        public List<Component.Component> Components = new List<Component.Component>();
        
        public Component.Transform transform { get; set; }
        public Component.RigidBody rigidBody { get; set; }

        public abstract GameObject Draw();

        
        protected GameObjectNewGen(List<GameObject> gameObjects)
        {
            
            transform = new Component.Transform
            {
                Tag = "Transform",
                LocalBasis =
                {
                    zAxis = Vector3.forward,
                    yAxis = Vector3.up,
                    xAxis = Vector3.right
                },
                GameObjectNewGen = this,
            };
            rigidBody = new RigidBody()
            {
                GameObjectNewGen = this,
                Tag = "RigidBody"
            };
            
            GameObjects = gameObjects;

            foreach (var gameObject in GameObjects)
            {
                 GameObjectsNormalizedPosition.Add(gameObject.transform.position);
                 GameObjectsInitialScale.Add(gameObject.transform.localScale);
            }
            
            
        }

       

        public void Update()
        {
            foreach (var component in Components)
            {                    
                component.Update();
                if (component.HasAForce)
                {
                  //  rigidBody.ApplyLinearForce(component.Force);
                    //Debug.Log(component.Force);
                    Vector3 v = (new Matrix(transform.LocalBasis) * new Matrix(((LinearJoint)component).ConnectedAnchor)).ToVector3();
                    rigidBody.ApplyRotationalForces(v,component.Force);
                }
            }
            rigidBody.Update();
            transform.Update();
        }
  
    }
}
