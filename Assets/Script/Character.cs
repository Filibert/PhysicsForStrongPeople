﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Script.Helper;
using Script.Model.Math;
using UnityEngine;

namespace Assets.Script
{
    public class Character: GameObjectNewGen
    {
        private GameObject O, X, Y, Z;
        private Vector3 _posH, _posL, _posR, _posB;

        public Character(List<GameObject> components) : base(components)
        {
            GameObjects = components;
            rigidBody.RotationalDamping = 0.9f;
            
            rigidBody.Mass = 5;
            rigidBody.Gravity = false;
            rigidBody.LinearDamping = 0.8f;
            rigidBody.RotationalDamping = 0.5f;
            rigidBody.ObjectInertia = new Matrix(3,3,new List<float>(){
                KnownInertia.RectangularBlockInertiaMatrix(4.2f,4f,1,rigidBody.Mass).MatrixRows[0][0] - transform.Position.x, 0, 0,
                0, KnownInertia.RectangularBlockInertiaMatrix(4.2f,4f,1,rigidBody.Mass).MatrixRows[1][1] - transform.Position.y, 0,
                0, 0, KnownInertia.RectangularBlockInertiaMatrix(4.2f,4f,1,rigidBody.Mass).MatrixRows[2][2] - transform.Position.z

            });
            
            
            //init position         
            transform.Position = new Func<Vector3>(() =>
            {
                float x = 0, y = 0, z = 0;
                foreach (GameObject component in GameObjects)
                {
                    if (component.name == "Head") 
                        y += component.transform.position.y;
                    if (component.name == "Arm")
                        x += component.transform.position.x;
                    if (component.name == "Body")
                    {
                        y -= component.transform.position.y;
                        z += component.transform.position.z;
                    }

                }
                return new Vector3(x/2, y, z);
            }).Invoke();
           
        }

        public override GameObject Draw()
        {
            throw new NotImplementedException();
        }
    }
}
