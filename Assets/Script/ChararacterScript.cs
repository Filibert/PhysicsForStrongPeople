﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Script.Component.Joint;
using Assets.Script.Physics;
using Script;
using Script.Helper;
using Script.Model.Math;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using SpringJoint = Assets.Script.Component.Joint.SpringJoint;

namespace Assets.Script
{
    public class ChararacterScript : MonoBehaviour
    {
        private Character _charactereModel;
        private GoGoGadgetoHand _gogoGadgetoHand, _pikachu;
        public GameObject Head, Body, ArmLeft, ArmRight, GogoGadgetoHand, pikachu;
        public List<GoGoGadgetoHand> PikachuArmy = new List<GoGoGadgetoHand>();
        private CollisionDetector detect;

            // Use this for initialization
        void Start()
        {
            Matrix m = KnownInertia.RectangularBlockInertiaMatrix(5, 5, 5, 50);
            _charactereModel = new Character(new List<GameObject>()
            {
                Head,
                Body,
                ArmLeft,
                ArmRight

            });

            for (int i = 0; i < 100; i++)
            {
                GameObject pikachuClone = Instantiate(pikachu, UnityEngine.Random.insideUnitCircle * 25, UnityEngine.Random.rotation);
                pikachuClone.transform.position = new Vector3(pikachuClone.transform.position.x,
                    pikachuClone.transform.position.y, 1);
                PikachuArmy.Add(new GoGoGadgetoHand(new List<GameObject>(){pikachuClone}){});
                PikachuArmy[i].rigidBody.Gravity = Convert.ToBoolean(i % 3);
            }

            
            _gogoGadgetoHand = new GoGoGadgetoHand(new List<GameObject>() {GogoGadgetoHand});

            _gogoGadgetoHand.rigidBody.Mass = 60;    
            _pikachu = new GoGoGadgetoHand(new List<GameObject>() {pikachu});
            _pikachu.rigidBody.Gravity = false;
            
          LinearJoint s = new LinearJoint(2f, _gogoGadgetoHand, new Vector3(3.1f,2.6f,1) - _charactereModel.transform.Position, new Vector3(-0.5f,0,0),_charactereModel);
           _gogoGadgetoHand.Components.Add(s);
            detect = new CollisionDetector();
            
        }
       
        // Update is called once per frame  
        void Update () {


            if (Input.GetKey(KeyCode.W))
            {
                Vector3 v = (new Matrix(_charactereModel.transform.LocalBasis) * new Matrix(new Vector3(0, 4, 0))).ToVector3();
                _charactereModel.rigidBody.Acceleration += v;
            }

            if (Input.GetKey(KeyCode.S))
            {
                Vector3 v = (new Matrix(_charactereModel.transform.LocalBasis) * new Matrix(new Vector3(0, -4, 0))).ToVector3();
                _charactereModel.rigidBody.Acceleration += v;
            }

            if (Input.GetKey(KeyCode.A))
            {
                _charactereModel.transform.Rotate(0, 0, 2f + _charactereModel.transform.EulerAngle.z);
            }

            if (Input.GetKey(KeyCode.D))
            {
                _charactereModel.transform.Rotate(0, 0, -2f + _charactereModel.transform.EulerAngle.z);
            }
            _charactereModel.Update();
          _gogoGadgetoHand.Update();
            _pikachu.Update();

            detect.SphereAndSphere(_gogoGadgetoHand, _pikachu, null);
            foreach (var pika in PikachuArmy)
            {
                detect.SphereAndSphere(_gogoGadgetoHand, pika, null);
                pika.Update();
            }
            ;
        }
    }
}
