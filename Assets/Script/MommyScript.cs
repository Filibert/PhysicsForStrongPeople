﻿using System.Collections;
using System.Collections.Generic;
using Script.Helper;
using UnityEngine;

namespace Assets.Script
{
    public class MommyScript : MonoBehaviour
    {
        private GameObject gogo, box;

        private GoGoGadgetoHand _gogo;
        private Box _box;
        private CollisionDetector detect;

        // Use this for initialization
        void Awake()
        {
            for (int i = 0; i < transform.childCount ; i++)
            {
                if (transform.GetChild(i).name == "gogo")
                {
                    gogo = transform.GetChild(i).gameObject;
                }
                
                if (transform.GetChild(i).name == "box")
                {
                    box = transform.GetChild(i).gameObject;
                }
            }

            Debug.Log(box);
            _gogo = new GoGoGadgetoHand(new List<GameObject>() { gogo });
            _gogo.rigidBody.Gravity = false;

            _box = new Box(new List<GameObject>() { box });

            detect = new CollisionDetector();

            Vector3 v = new Vector3(0, -0.02f, 0);
            _box.rigidBody.Velocity = v;
        }

        // Update is called once per frame
        void Update()
        {
            _gogo.Update();
            _box.Update();

            detect.SphereAndBox(_gogo, _box, null);

        }
    }
}