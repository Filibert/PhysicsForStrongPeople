﻿using System.Collections.Generic;
using Assets.Script;
using Assets.Script.Physics;
using Script.Helper;
using Script.Model.Math;
using UnityEngine;

namespace Assets.Script
{
    public class GoGoGadgetoHand : GameObjectNewGen
    {
        public SpringForce SpringForce;
        public GoGoGadgetoHand(List<GameObject> gameObjects) : base(gameObjects)
        {
            GameObjects = gameObjects;
            transform.Position = gameObjects[0].transform.position;
            rigidBody.Mass = 5f;
            rigidBody.Gravity = true;
            rigidBody.LinearDamping = 0.1f;
            rigidBody.RotationalDamping = 0.4f;
            rigidBody.ObjectInertia = new Matrix(3, 3, new List<float>()
            {
                    KnownInertia.RectangularBlockInertiaMatrix(1f, 1f, 1, rigidBody.Mass).MatrixRows[0][0],
                0,
                0,
                0,
                KnownInertia.RectangularBlockInertiaMatrix(1f, 1f, 1, rigidBody.Mass).MatrixRows[1][1],
                0,
                0,
                0,
                KnownInertia.RectangularBlockInertiaMatrix(1f, 1f, 1, rigidBody.Mass).MatrixRows[2][2]
            });

            rigidBody.RestitutionCoeff = 0.01f;
            //  Rigidbody2D
        }

        public override GameObject Draw()
        {
            throw new System.NotImplementedException();
        }
    }
}