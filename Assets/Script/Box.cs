﻿using System.Collections.Generic;
using Assets.Script;
using Assets.Script.Physics;
using Script.Helper;
using Script.Model.Math;
using UnityEngine;

namespace Assets.Script
{
    public class Box : GameObjectNewGen
    {
        public SpringForce SpringForce;
        public Box(List<GameObject> gameObjects) : base(gameObjects)
        {
            GameObjects = gameObjects;
            transform.Position = gameObjects[0].transform.position;
            rigidBody.Mass = 5;
            Debug.Log(rigidBody.InverseMass);
            rigidBody.Gravity = false;

            rigidBody.RestitutionCoeff = 1;

            rigidBody.LinearDamping = 0.8f;
            rigidBody.RotationalDamping = 0.00001f;
            rigidBody.ObjectInertia = new Matrix(3, 3, new List<float>(){
                KnownInertia.SphereWithCavicalInertiaMatrix(1,1,rigidBody.Mass).MatrixRows[0][0] - transform.Position.x, 0, 0,
                0, KnownInertia.SphereWithCavicalInertiaMatrix(1,1,rigidBody.Mass).MatrixRows[1][1] - transform.Position.y, 0,
                0, 0, KnownInertia.SphereWithCavicalInertiaMatrix(1,1,rigidBody.Mass).MatrixRows[2][2] - transform.Position.z

            });

            //  Rigidbody2D
        }

        public override GameObject Draw()
        {
            throw new System.NotImplementedException();
        }
    }
}