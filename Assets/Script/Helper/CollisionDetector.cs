﻿using Assets.Script;
using Assets.Script.Physics;
using UnityEngine;

namespace Script.Helper
{   
    
    public class CollisionDetector
    {
        public void SphereAndSphere(
            GameObjectNewGen gameObject1,
            GameObjectNewGen gameObject2,
            CollisionData data)
        {
            Vector3 pos1 = gameObject1.transform.Position;
            Vector3 pos2 = gameObject2.transform.Position;
            
            Vector3 midLine = pos1 - pos2;

            float size = midLine.magnitude;

            //Debug.Log(size);

            if (size <= 0.0f || size >= gameObject1.transform.Scale.x)
            {
                return;
            }
            
            Vector3 normal = midLine * (1.0f / size);

            Contact contact = new Contact();

            contact.contactNormal = normal;
            contact.contactPoint = pos1 + midLine * 0.5f;
            contact.penetration = gameObject1.transform.Scale.x +
                gameObject2.transform.Scale.x - size;

            contact.gameObjects.Add(gameObject1);
            contact.gameObjects.Add(gameObject2);

            //Debug.Log("Collision between " + gameObject1 + " and" + gameObject2);

            contact.ResolveVelocity();
            //    contact.ResolveInterpenetration();
        }

        public void SphereAndBox(
            GameObjectNewGen sphere,
            GameObjectNewGen box,
            CollisionData data)
        {
            Vector3 center = sphere.transform.Position;
            Vector3 relCenter = box.transform.TransformInverse(center);

            if (Mathf.Abs(relCenter.x) - sphere.transform.Scale.x > box.transform.Scale.x / 2 ||
                Mathf.Abs(relCenter.y) - sphere.transform.Scale.y > box.transform.Scale.y / 2 ||
                Mathf.Abs(relCenter.z) - sphere.transform.Scale.z > box.transform.Scale.z / 2)
            {
                return;
            }

            Vector3 closestPt = new Vector3();
            float dist;

            dist = relCenter.x;
            if (dist > box.transform.Scale.x / 2) dist = box.transform.Scale.x / 2;
            if (dist < box.transform.Scale.x / 2) dist = -box.transform.Scale.x / 2;
            closestPt.x = dist;

            dist = relCenter.y;
            if (dist > box.transform.Scale.y / 2) dist = box.transform.Scale.y / 2;
            if (dist < box.transform.Scale.y / 2) dist = -box.transform.Scale.y / 2;
            closestPt.y = dist;

            dist = relCenter.z;
            if (dist > box.transform.Scale.z / 2) dist = box.transform.Scale.z / 2;
            if (dist < box.transform.Scale.z / 2) dist = -box.transform.Scale.z / 2;
            closestPt.z = dist;

            dist = (closestPt - relCenter).sqrMagnitude;

            if (dist > sphere.transform.Scale.x * sphere.transform.Scale.x)
            {
                return;
            }

            Contact contact = new Contact();

            contact.contactNormal = (center - closestPt);
            contact.contactNormal.Normalize();
            contact.contactPoint = closestPt;
            contact.penetration = sphere.transform.Scale.x - dist * dist;

            contact.gameObjects.Add(sphere);
            contact.gameObjects.Add(box);

            contact.ResolveVelocity();
            //contact.ResolveInterpenetration();

        }
    }
}

