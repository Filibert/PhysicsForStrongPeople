﻿using System.Collections.Generic;
using Script.Model.Math;
using UnityEngine;

namespace Script.Helper
{
    public static class KnownInertia
    {
        public static Matrix RectangularBlockInertiaMatrix(float width, float height,float depht, float mass)
        {
            return new Matrix(3, 3, new List<float>()
            {
                1f/12f*mass*(depht*depht + height*height), 0, 0,
                0, 1f/12f*mass*(width*width + depht*depht), 0,
                0, 0, 1f/12f*mass*(width*width + height*height)
            });
        }

        public static Matrix FulledSphereInertiaMatrix(float r, float mass)
        {
            return new Matrix(3, 3, new List<float>()
            {
                (2f*mass*r*r)/5f, 0, 0,
                0,(2f*mass*r*r)/5f, 0,
                0, 0,(2f*mass*r*r)/5f
            }).Inverse();
        }
        public static Matrix SphereWithCavicalInertiaMatrix(float r1,float r2, float mass)
        {
            return new Matrix(3, 3, new List<float>()
            {
                2f * mass *( Mathf.Pow(r2, 5f) - Mathf.Pow(r1, 5f)) / ( 5f * (Mathf.Pow(r2, 3f) - Mathf.Pow(r1, 3f))), 0, 0,
                0, 2f * mass *( Mathf.Pow(r2, 5f) - Mathf.Pow(r1, 5)) / ( 5f * (Mathf.Pow(r2, 3f) - Mathf.Pow(r1, 3f))), 0,
                0, 0, 2f * mass *( Mathf.Pow(r2, 5f) - Mathf.Pow(r1, 5)) / ( 5f * (Mathf.Pow(r2, 3) - Mathf.Pow(r1, 3f)))
            }).Inverse();
        }
    }
    
}