﻿using System.Collections.Generic;
using Script.Model.Math;
using UnityEngine;

namespace Assets.Script.Helper
{
    public static class RotationHelper
    {
        private static Vector3 Rotation(Matrix rotation, Vector3 target)
        {
            var currentPosition = new Matrix(target);
            Matrix result = rotation*currentPosition;
            float x = result.MatrixColumns[0][0];
            float y = result.MatrixColumns[0][1];
            float z = result.MatrixColumns[0][2];
        
            return new Vector3(x, y, z);
        }
        public static Vector3 Rotation(float angle, Vector3 target, Vector3 rotationAxe)
        {
            angle = Mathf.Deg2Rad * angle;
            var p = new Matrix(3, 3, new List<float>()
            {   
                rotationAxe.x * rotationAxe.x, rotationAxe.x * rotationAxe.y, rotationAxe.x * rotationAxe.y,
                rotationAxe.y * rotationAxe.x, rotationAxe.y * rotationAxe.y, rotationAxe.y * rotationAxe.z,
                rotationAxe.z * rotationAxe.x, rotationAxe.z * rotationAxe.y, rotationAxe.z * rotationAxe.z       
            });
            var id = new Matrix(3, 3, new List<float>()
            {
                1, 0, 0,
                0, 1, 0,
                0, 0, 1
            });    
            var q = new Matrix(3, 3, new List<float>()
            {
                0, -rotationAxe.z, rotationAxe.y,
                rotationAxe.z, 0, -rotationAxe.x,
                -rotationAxe.y, rotationAxe.x, 0
               
              
            });
            var rotation = p + Mathf.Cos(angle) * (id - p)  + Mathf.Sin(angle)*q;
            return Rotation(rotation, target);
        }   
    
        public static Vector3 RotationX(float angle, Vector3 target)
        {
            angle = Mathf.Deg2Rad * angle;
            var rotation = new Matrix(3, 3, new List<float>() { 1, 0, 0, 0, Mathf.Cos(angle), -Mathf.Sin(angle), 0, Mathf.Sin(angle), Mathf.Cos(angle) });
            return Rotation(rotation,target);
        }
        public static Vector3 RotationY(float angle, Vector3 target)
        {
            angle = Mathf.Deg2Rad * angle;
            var rotation = new Matrix(3, 3, new List<float>() { Mathf.Cos(angle), 0, Mathf.Sin(angle), 0, 1, 0, -Mathf.Sin(angle), 0, Mathf.Cos(angle) });
            return Rotation(rotation,target);
        }
        public static Vector3 RotationZ(float angle, Vector3 target)
        {
            angle = Mathf.Deg2Rad * angle;
            var rotation = new Matrix(3, 3, new List<float>() { Mathf.Cos(angle), -Mathf.Sin(angle), 0, Mathf.Sin(angle), Mathf.Cos(angle),0,0,0,1 });
            return Rotation(rotation, target);
        }        
        
        public static OrthonormalBasis RotateAndChangeOrientation(float x, float y, float z, OrthonormalBasis parentBasis, GameObject target)
        {
            var newBase = Rotate(x, y, z, parentBasis);
            target.transform.localRotation = UnityEngine.Quaternion.LookRotation(newBase.zAxis,newBase.yAxis);
            return newBase;
        }
        public static OrthonormalBasis Rotate(float x, float y, float z, OrthonormalBasis parentBasis)
        {
            Vector3 up = parentBasis.yAxis;
            Vector3 forward = parentBasis.zAxis;
            Vector3 right = parentBasis.xAxis;
        
            //rotate arround Z
            up = RotationZ(z, up);
            right = RotationZ(z, right);
        
        
            //rotate arround x
            up = RotationX(x, up);
            forward = RotationX(x, forward);
            right = RotationX(x, right);
        
            //rotate arround y
            right = RotationY(y, right);
            forward = RotationY(y, forward);
            up = RotationY(y, up);
        
            return new OrthonormalBasis(){
                xAxis = right,
                yAxis = up,
                zAxis = forward
            };
        }
        
        public static string PrintListVector3(List<Vector3> v, List<Vector3> j)
        {
            string s = string.Empty;
            int i = 0;
            foreach (var value in v)
            {
                s += (value - j[i]) + " | ";
                i++;
            }
            return s;
        }

    }
}