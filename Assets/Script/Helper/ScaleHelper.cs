﻿using System.Collections.Generic;
using Script.Model.Math;
using UnityEngine;

namespace Assets.Script.Helper
{
    public static class ScaleHelper
    {
        public static Vector3 ScalePosition(Vector3 scaleValue, Vector3 position)
        {
            Matrix scalingMatrix = new Matrix(new List<List<float>>()
            {
                new List<float>() {scaleValue.x, 0, 0},
                new List<float>() {0, scaleValue.y, 0},
                new List<float>() {0, 0, scaleValue.z}
            });
            Vector3 newPosition = (scalingMatrix * new Matrix(position)).ToVector3();
            return newPosition;
        }
    }
}