﻿namespace Assets.Script
{
    public enum CollisionType
    {
        Sphere,
        AAB,
        DOP
    }
}