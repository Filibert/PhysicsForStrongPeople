﻿using System.Collections.Generic;
using System.Linq;
using Assets.Script;
using UnityEngine;

namespace Script.Model.Math
{
    public class Matrix
    {
        
        private readonly Size Size;
        public List<List<float>> MatrixRows { get; set; }
        public List<List<float>> MatrixColumns { get; set; }

        public Matrix(int n, int m)
        {
            Size = new Size(n,m);
            MatrixRows = new List<List<float>>();
            MatrixColumns = new List<List<float>>();

            var zeros = new List<float>();
            for(var i = 0; i < m*n; i++)
                zeros.Add(0);
            InitRow(zeros);
            InitColumn();
        }
        public Matrix(int n, int m, List<float> elements)
        {

            MatrixRows = new List<List<float>>();
            MatrixColumns = new List<List<float>>();
            Size = new Size(n, m);
            InitRow(elements);
            InitColumn();
        }
        public Matrix(Vector3 v)
        {
            Size = new Size(1,3);
            MatrixRows = new List<List<float>>()
            {
                new List<float>(){ v.x },
                new List<float>(){ v.y },
                new List<float>(){ v.z },
            };
            MatrixColumns = new List<List<float>>()
            {
                new List<float>()
                {
                    v.x,
                    v.y,
                    v.z
                }
            };       
        }
        public Matrix(OrthonormalBasis o)
        {
            Size = new Size(3,3);
            MatrixRows = new List<List<float>>()
            {
                new List<float>(){ o.xAxis.x, o.yAxis.x, o.zAxis.x },
                new List<float>(){ o.xAxis.y, o.yAxis.y, o.zAxis.y },
                new List<float>(){ o.xAxis.z, o.yAxis.z, o.zAxis.z }
            };
            MatrixColumns = new List<List<float>>()
            {
                new List<float>(){ o.xAxis.x, o.xAxis.y, o.xAxis.z },
                new List<float>(){ o.yAxis.x, o.yAxis.y, o.yAxis.z },
                new List<float>(){ o.zAxis.z, o.zAxis.y, o.zAxis.z  },
            };       
        }

        public Matrix(List<List<float>> rows)
        {
            Size = new Size(rows[0].Count, rows.Count);
            MatrixColumns = new List<List<float>>();
            MatrixRows = rows;
            InitColumn();
        }
        
        private void InitRow(List<float> f)
        {
            for (var j = 0; j < Size.Height; j++)
            {
                var row = new List<float>();
                for (var i = 0; i < Size.Width; i++)
                {
                    row.Add(f[Size.Width * j + i]);
                }
                MatrixRows.Add(row);
            }
        }
        private void InitColumn()
        {
            for (var i = 0; i < Size.Width; i++)
            {
                var column = MatrixRows.Select(rows => rows[i]).ToList();
                MatrixColumns.Add(column);
            }   
        }

        //par soucis de temps nous utiliserons l'inversion matricielle de unity

        public Matrix Inverse()
        {

            Matrix4x4 m = new UnityEngine.Matrix4x4();
            m.SetRow(0, new Vector4(MatrixColumns[0][0], MatrixColumns[0][1], MatrixColumns[0][2], 0));
            m.SetRow(1, new Vector4(MatrixColumns[1][0], MatrixColumns[1][1], MatrixColumns[1][2], 0));
            m.SetRow(2, new Vector4(MatrixColumns[2][0], MatrixColumns[2][1], MatrixColumns[2][2], 0));
            m.SetRow(3, new Vector4(0, 0, 0, 1));
            
            m = m.inverse;
            
            return new Matrix(3,3,new List<float>()
            {
                m.m00,m.m01,m.m02,
                m.m10,m.m11,m.m12,
                m.m20,m.m21,m.m22
            });

        }
        
        public static Matrix operator * (Matrix m1, Matrix m2)
        {
            var j = new List<List<float>>();
            foreach (var row in m1.MatrixRows)
            {
                var newRows = new List<float>();
                foreach (var column in m2.MatrixColumns)
                {
                    float superValue = 0;
                    for (var i = 0; i < m1.Size.Width; i++)
                    {
                        superValue += row[i] * column[i];
                    }
                    newRows.Add(superValue);
                }
                j.Add(newRows);
            }
            return new Matrix(j);
        }
        public static Matrix operator *(float scalar, Matrix m1)
        {
            var newElements = (from row in m1.MatrixRows from x in row select x * scalar).ToList();

            return new Matrix(m1.Size.Width,m1.Size.Height,newElements);
        }
        public static Matrix operator - (Matrix m1, Matrix m2)
        {
            var newElements = new List<float>();
            int i = 0, j = 0;

            foreach (var row in m1.MatrixRows)
            {
                
                foreach (var x in row)
                {
                    Debug.Log(i + " : " + j);
                    newElements.Add(x - m2.MatrixRows[i][j]);
                    j = ( j + 1 ) % m2.Size.Width;
                }
                i++;
            }

            return new Matrix(m1.Size.Width, m1.Size.Height, newElements);
        }
        public static Matrix operator + (Matrix m1, Matrix m2)
        {
            var newElements = new List<float>();
            int i = 0, j = 0;

            foreach (var row in m1.MatrixRows)
            {

                foreach (var x in row)
                {
                    newElements.Add(x + m2.MatrixRows[i][j]);
                    j = (j + 1) % m2.Size.Width;
                }
                i++;
            }

            return new Matrix(m1.Size.Width, m1.Size.Height, newElements);
        }
        
         
        public override string ToString()
        {
            var s = "";
            foreach (var row in MatrixRows)
            {
                s = row.Aggregate(s, (current, element) => current + (element + "|"));
                s += "\n";
            }
            return s;
        }

        public Vector3 ToVector3()
        {
            var v = new Vector3(MatrixColumns[0][0], MatrixColumns[0][1], MatrixColumns[0][2]);
            return v;
        }
    }
}
