﻿using UnityEngine;

namespace Script.Model.Math
{
    public class OrthonormalBasis
    {
        public Vector3 xAxis, yAxis, zAxis;
        
        public OrthonormalBasis()
        {
            this.xAxis = Vector3.right;
            this.yAxis = Vector3.up;
            this.zAxis = Vector3.forward;
        }
        public OrthonormalBasis(Vector3 xAxis, Vector3 yAxis, Vector3 zAxis)
        {
            this.xAxis = xAxis;
            this.yAxis = yAxis;
            this.zAxis = zAxis;
        }
        
    }
}