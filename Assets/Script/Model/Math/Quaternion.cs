﻿using System;

namespace Script.Model.Math
{
    public class Quaternion
    {
        public float w,x,y,z;
        
        public Quaternion(float w, float x, float y, float z)
        {
            this.w = w;
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Quaternion LookForward(float y, float z)
        {
            throw new NotImplementedException();
        }
        
        public override string ToString()
        {
            return base.ToString();
        }
    }
}