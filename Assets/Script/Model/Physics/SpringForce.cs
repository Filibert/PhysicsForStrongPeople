﻿using UnityEngine;
using UnityEngine.Assertions.Comparers;

namespace Assets.Script.Physics
{
    public class SpringForce : Force
    {
        public float K,C;
        public float RestingLentgh = 0;
        public float ForceMagnitude = 0;
        public Vector3 Velocity;
             
        public float ActualLentgh{
            get { return ActualLentgh; }
            set
            {
              //  ActualLenght = value;
                ForceMagnitude = K * (RestingLentgh - value);
            }
        }
        
        public void Init()
        {
            
        }

        public void Calculate()
        {
            throw new System.NotImplementedException();
        }

        public void Update()
        {
            throw new System.NotImplementedException();
        }
    }
}