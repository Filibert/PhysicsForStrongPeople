﻿using UnityEngine;

namespace Assets.Script.Physics
{
    public class Torque
    {
        public Vector3 ImpactPoint = Vector3.zero;
        public Vector3 Force = Vector3.zero;

        public Vector3 TorqueVector
        {
            get
            {
                return Vector3.Cross(ImpactPoint,Force);
            }
            set
            {
                TorqueVector = value;
                
            }
        }
    }
}