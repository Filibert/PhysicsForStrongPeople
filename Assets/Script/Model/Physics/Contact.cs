﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.Physics
{
    public class Contact 
    {
        public Vector3 contactPoint { get; set; }
        public Vector3 contactNormal { get; set; }
        public float penetration { get; set; }

        public List<GameObjectNewGen> gameObjects = new List<GameObjectNewGen>();

        protected float CalculateSeparatingVelocity()
        {
            Vector3 relativeVelocity = gameObjects[0].rigidBody.Velocity;
            if (gameObjects[1] != null)
            {
                relativeVelocity -= gameObjects[1].rigidBody.Velocity;
            }
            //Debug.Log("relVel " + relativeVelocity + " contactNormal " + contactNormal);
            return Vector3.Dot(relativeVelocity, contactNormal);
        }

        public void ResolveVelocity()
        {
            float separatingVelocity = CalculateSeparatingVelocity();
            // Debug.Log(separatingVelocity);
            if (separatingVelocity > 0)
            {
                return;
            }

            float newSeparationVelocity = -separatingVelocity * gameObjects[0].rigidBody.RestitutionCoeff;

            float deltaVelocity = newSeparationVelocity - separatingVelocity;


            float totalInverseMass = gameObjects[0].rigidBody.InverseMass;

            if (gameObjects[1] != null)
            {
                totalInverseMass += gameObjects[1].rigidBody.InverseMass;
            }

            if (totalInverseMass <= 0)
            {
                return;
            }

            float impulse = deltaVelocity / totalInverseMass;

            Vector3 impulsePerIMass = contactNormal * impulse;

            

            gameObjects[0].rigidBody.Velocity = (impulsePerIMass * gameObjects[0].rigidBody.InverseMass);

            if (gameObjects[1] != null)
            {
                gameObjects[1].rigidBody.Velocity = (impulsePerIMass * -gameObjects[1].rigidBody.InverseMass);
                
            }
        }

        public void ResolveInterpenetration()
        {
            if (penetration <= 0)
                return;

            float totalInverseMass = gameObjects[0].rigidBody.InverseMass;

            if (gameObjects[1] != null)
            {
                totalInverseMass += gameObjects[1].rigidBody.InverseMass;
            }

            if (totalInverseMass <= 0)
            {
                return;
            }

            Vector3 movePerIMass = contactNormal * (-penetration / totalInverseMass);

            gameObjects[0].transform.Position += movePerIMass * gameObjects[0].rigidBody.InverseMass;

            if (gameObjects[1] != null)
            {
                gameObjects[1].transform.Position += movePerIMass * gameObjects[1].rigidBody.InverseMass;
            }

        }
    }
}
