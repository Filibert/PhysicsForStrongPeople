﻿namespace Assets.Script.Physics
{
    public interface Force
    {
        void Init();
        void Calculate();
        void Update();
    }
}