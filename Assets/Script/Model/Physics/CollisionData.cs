﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.Physics
{
    public class CollisionData
    {
        // Holds the contact array to write into.
        public List<Contact> contacts = new List<Contact>();

        // Holds the maximum number of contacts the array can take.
        private int maximumContacts { get; set; }

        
    }
}
