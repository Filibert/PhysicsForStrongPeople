﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Script
{
    public class Size
    {
        public int Width;
        public int Height;

        public Size(int w, int h)
        {
            Width = w;
            Height = h;
        }
    }
}
